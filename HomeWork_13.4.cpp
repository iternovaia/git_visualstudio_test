﻿// HomeWork_13.4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <math.h>
#include <string>
#include "Helpers.h"

using namespace std;

class Player
{
public:
    std::string name;
    int score;

Player()
{
    string	name = "Default";
    int score = 0;
}
        
Player(string N, int S)
{
    name = N;
    score = S;
}

};

void bubbleSort(Player* players, int size)
{
    for (int i = 0; i < size - 1; i++)
    {
        for (int j = 0; j < size - i - 1; j++)
        {
            if (players[j].score < players[j + 1].score)
            {
                Player temp = players[j];
                players[j] = players[j + 1];
                players[j + 1] = temp;
            }
        }
    }
}

int main()
{
int size;
std::cout << "Enter number of players: ";   // Ввод кол-ва игроков
std::cin >> size;

Player* arr = new Player[size]; // указатель на массив игроков созданный в куче

    for (int i = 0; i < size; i++) // Заполняем массив
    {
        std::string name;
        int score;
        std::cout << "Enter name and score for player " << i + 1 << ": ";
        std::cin >> name >> score;
        arr[i] = Player(name, score);
    }

bubbleSort(arr, size); // Применяем функцию сортировки

std::cout << "Sorted players:\n";  // Выводм в консоль

    for (int i = 0; i < size; i++)
    {
        std::cout << arr[i].name << ": " << arr[i].score << std::endl;
    }

delete[] arr; // Чистим память

return 0;
}
